#include <stdio.h>
#include "dice.h"
int main(){
    int n;
    //create a seed for the random function used in the rollDice()
    initializeSeed();
    printf("insert the number of faces of the dice: ");
    scanf("%d",&n);
    //generates a random number from 1 to n
    printf("Let's roll the dice: %d\n",rollDice(n));
    return 0;
}
